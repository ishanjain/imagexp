extern crate clap;
extern crate image;

mod algos;

use image::{ColorType, DynamicImage, GenericImage, Pixel};
use std::{env, ffi::OsStr, fs::File, io::prelude::*, io::Cursor, path::Path};



fn main() {
    let cli_args: Vec<String> = env::args().collect();

    if cli_args.len() == 0 {
        println!("Please provide the source image name");
        return;
    }

    println!("Processing {}", cli_args[1]);

    let input_img_addr = Path::new(&cli_args[1]);

    let input_ext = match input_img_addr.extension() {
        Some(v) => v,
        None => panic!("file has no extension"),
    };

    let file_content = read_file(input_img_addr);

    let mut img = load_image(file_content, input_ext);

    let mut proc_img = DynamicImage::new_luma_a8(img.dimensions().0, img.dimensions().1);

    for (x, y, pixel) in img.enumerate_pixels() {
        proc_img.put_pixel(x, y, algos::grayscale::basic_grayscale(*pixel));
    }


    proc_img.save(
        input_img_addr
            .with_file_name(format!(
                "{}_{}",
                input_img_addr.file_stem().unwrap().to_str().unwrap(),
                "mod"
            )).with_extension("png"),
    );
}

fn read_file(file_path: &Path) -> Vec<u8> {
    let mut file = File::open(file_path).expect("file not found");
    let mut content = vec![];

    file.read_to_end(&mut content)
        .expect("error in reading file content");
    return content;
}

#[cfg(target_arch = "wasm32")]
fn load_image() {}

#[cfg(not(target_arch = "wasm32"))]
fn load_image(content: Vec<u8>, input_ext: &OsStr) -> image::RgbaImage {
    match input_ext.to_str().unwrap() {
        "PNG" | "png" => image::load(Cursor::new(content), image::PNG)
            .unwrap()
            .to_rgba(),
        "JPEG" | "JPG" | "jpeg" | "jpg" => image::load(Cursor::new(content), image::JPEG)
            .unwrap()
            .to_rgba(),
        &_ => panic!("This program only works with JPEG or PNG files"),
    }
}
