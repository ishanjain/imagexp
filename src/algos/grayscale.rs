use image::Rgba;
use std::cmp;

///! Basic Grayscale takes average of all three RGB values. A is kept intact.
pub fn basic_grayscale(color: Rgba<u8>) -> Rgba<u8> {
    let v = ((color[0] as f32 + color[1] as f32 + color[2] as f32) / 3.0) as u8;

    Rgba {
        data: [v, v, v, color[3]],
    }
}

///! In Improved Grayscale, RGB values are multiplied by known constants. These constants are
///supposed to make the grayscaled image look better to human eyes
pub fn improved_grayscale(color: Rgba<u8>) -> Rgba<u8> {
    let v = ((color[0] as f32 * 0.3 + color[1] as f32 * 0.59 + color[2] as f32 * 0.11) / 3.0) as u8;

    Rgba {
        data: [v, v, v, color[3]],
    }
}

pub fn desaturation(color: Rgba<u8>) -> Rgba<u8> {
    let v = ((max_of_three(color[0], color[1], color[2])
        + min_of_three(color[0], color[1], color[2])) as f32 / 2.0) as u8;

    Rgba {
        data: [v, v, v, color[3]],
    }
}

pub fn decomposition_max(color: Rgba<u8>) -> Rgba<u8> {
    let v = max_of_three(color[0], color[1], color[2]);
    Rgba {
        data: [v, v, v, color[3]],
    }
}

pub fn decomposition_min(color: Rgba<u8>) -> Rgba<u8> {
    let v = min_of_three(color[0], color[1], color[2]);
    Rgba {
        data: [v, v, v, color[3]],
    }
}

///! Single channel {Red, Green, Blue} uses the corresponding {Red, Green, Blue} value as the
///Grayscale value
pub fn single_channel_red(color: Rgba<u8>) -> Rgba<u8> {
    let v = color[0];
    Rgba {
        data: [v, v, v, color[3]],
    }
}

///! Single channel {Red, Green, Blue} uses the corresponding {Red, Green, Blue} value as the
///Grayscale value
pub fn single_channel_green(color: Rgba<u8>) -> Rgba<u8> {
    let v = color[1];
    Rgba {
        data: [v, v, v, color[3]],
    }
}

///! Single channel {Red, Green, Blue} uses the corresponding {Red, Green, Blue} value as the
///Grayscale value
pub fn single_channel_blue(color: Rgba<u8>) -> Rgba<u8> {
    let v = color[2];
    Rgba {
        data: [v, v, v, color[3]],
    }
}

fn max_of_three(x: u8, y: u8, z: u8) -> u8 {
    cmp::max(cmp::max(x, y), z)
}

fn min_of_three(x: u8, y: u8, z: u8) -> u8 {
    cmp::min(cmp::min(x, y), z)
}
